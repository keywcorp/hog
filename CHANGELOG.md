# 1.0.5
- remove white paper from docs

# 1.0.4
- add white paper to docs

# 1.0.3
- add hog logo svg to repo
- add CHANGELOG

# 1.0.2
- remove pig scripts from repo

# 1.0.1
- removed unused files

# 1.0.0
