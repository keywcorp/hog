# get node v6
FROM node:wheezy

# create hog directory
RUN mkdir -p /usr/src/hog
WORKDIR /usr/src/hog

# Bundle boar source
COPY . /usr/src/hog/

# expose port 900
EXPOSE 9000

CMD [ "npm", "start" ]
